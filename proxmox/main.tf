resource "null_resource" "tmp_directories" {

  provisioner "local-exec" {
    command = <<-EOF
      mkdir -p "${path.module}/tmpfiles/"
    EOF
  }

  provisioner "local-exec" {
    when    = destroy
    command = <<-EOF
      rm -rf "${path.module}/tmpfiles/"
    EOF
  }

}

data "template_file" "cloudinit_user_data" {
  count = length(var.infra-nodes)
  template = file("${path.module}/cloudinit_user.cfg")
    vars = {
    Domain = var.infra-network-domain
    Hostname = lookup(var.infra-nodes[count.index], "nodename")
  }
}

data "template_file" "cloudinit_net_data" {
  count = length(var.infra-nodes)
  template = file("${path.module}/cloudinit_net_${lookup(var.infra-nodes[count.index], "net_type", "dhcp")}.cfg")
  vars = {
    Domain = var.infra-network-domain
    NameServer = var.infra-network-nameserver
    Gateway = lookup(var.infra-nodes[count.index], "nodeip_gateway","use_dhcp")
    IPAddress = lookup(var.infra-nodes[count.index], "nodeip_with_mask", "use_dhcp")
  }
}

resource "local_file" "cloudinit_user_file" {
  depends_on = [
    null_resource.tmp_directories,
  ]


  filename = "${path.module}/tmpfiles/${lookup(var.infra-nodes[count.index], "nodename")}-cloudinit_user.cfg"
  count = length(var.infra-nodes)
  content = data.template_file.cloudinit_user_data[count.index].rendered
}

resource "local_file" "cloudinit_net_file" {
  depends_on = [
    null_resource.tmp_directories,
  ]
  
  filename = "${path.module}/tmpfiles/${lookup(var.infra-nodes[count.index], "nodename")}-cloudinit_net.cfg"
  count = length(var.infra-nodes)
  content = data.template_file.cloudinit_net_data[count.index].rendered
}

resource "null_resource" "cloudinit_files" {
  count = length(var.infra-nodes)
  connection {
    type     = "ssh"
    user     = var.proxmox-user
    password = var.proxmox-password
    host     = var.proxmox-server
  }

  provisioner "file" {
    source      = local_file.cloudinit_user_file[count.index].filename
    destination = "/var/lib/vz/snippets/${lookup(var.infra-nodes[count.index], "nodename")}-cloudinit_user.yml"
  }
  provisioner "file" {
    source      = local_file.cloudinit_net_file[count.index].filename
    destination = "/var/lib/vz/snippets/${lookup(var.infra-nodes[count.index], "nodename")}-cloudinit_net.yml"
  } 

}

resource "proxmox_vm_qemu" "nodes" {
  
  count = length(var.infra-nodes)

  name              = lookup(var.infra-nodes[count.index], "nodename")
  
  target_node       = var.proxmox-node
  
  agent = 1

  clone             = var.infra-os-base-template

  os_type           = "cloud-init"
  cores             = lookup(var.infra-nodes[count.index], "vcpu")
  sockets           = "1"
  cpu               = "host"
  memory            = lookup(var.infra-nodes[count.index], "mem_in_gb")*1024
  scsihw            = "virtio-scsi-pci"
  bootdisk          = "scsi0"

  disk {
    id              = 0
    size            = lookup(var.infra-nodes[count.index], "sysdisk_in_gb")
    type            = "scsi"
    storage         = var.proxmox-storage
    storage_type    = "lvm"
    iothread        = true
  }

  disk {
    id              = 1
    size            = lookup(var.infra-nodes[count.index], "datadisk_in_gb")
    type            = "scsi"
    storage         = var.proxmox-storage
    storage_type    = "lvm"
    iothread        = true
  }

  network {
    id              = 0
    model           = "virtio"
    bridge          = var.kvm_bridge_interface
  }

  lifecycle {
    ignore_changes  = [
      network,
    ]
  }

  # Cloud Init Settings
  
  cicustom = "user=local:snippets/${lookup(var.infra-nodes[count.index], "nodename")}-cloudinit_user.yml"

  ipconfig0         = "ip=${lookup(var.infra-nodes[count.index], "nodeip_with_mask")},gw=${lookup(var.infra-nodes[count.index], "nodeip_gateway")}"
  
  sshkeys = file(var.public_sshkey)

}
