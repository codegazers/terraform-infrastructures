variable "proxmox-server" {
  type = string
  description = "proxmox-server"
}

variable "proxmox-user" {
  type = string
  default = "root@pam"
  description = "proxmox-user"
}

variable "proxmox-password" {
  type = string
  #default = "password"
  description = "proxmox-password"
}

variable "proxmox-node" {
  type = string
  default = "localhost"
  description = "proxmox-node"
}

variable "proxmox-pool" {
  type = string
  default = "pool0"
  description = "proxmox-pool"
}

variable "proxmox-storage" {
  type = string
  default = "default"
  description = "proxmox-storage"
}

variable "proxmox-storage-type" {
  type = string
  default = "default"
  description = "proxmox-storage-type"
}


variable "infra-os-base-template" {
  type = string
  default = "template"
  description = "Infrastructure OS Base Image"
}

variable "public_sshkey" {
  type = string
  default = "../keys/provision"
  description = "SSH Private Key"
}

variable "infra-environment" {
  type = string
  default = "lab"
  description = "Infrastructure Environment Name"
}

variable "infra-network-name" {
  type = string
  default = "labs"
  description = "Network Name"
}

variable "infra-network-domain" {
  type = string
  default = "labs"
  description = "Nodes Network Domain"
}

variable "infra-network-nameserver" {
  type = string
  default = "8.8.8.8"
  description = "Nodes Network Nameserver"
}

variable "infra-network-subnet" {
  description = "Nodes' Subnet (For DHCP Only)"
  type = list(string)
  default = ["192.168.100.0/24"]
}

variable "infra-network-gateway" {
  type = string
  default = "192.168.100.1"
}


variable "infra-network-bridge" {
  description = "Enables libvirt Network Bridge Mode"
  type = bool
  default = false
}

variable "kvm_pool" {
  type = string
  default = "labs"
  description = "Pool for disk images and other resources (must be previously created)."

    # $ virsh pool-define-as --name labs --type dir --target /media/work/kvm
    # Pool default defined
    # Set pool to be started when libvirt daemons starts:

    # $ virsh pool-autostart labs
    # Pool default marked as autostarted
    # Start pool:

    # $ virsh pool-start labs
    # Pool default started

}

variable "kvm_bridge_interface" {
  type = string
  default = "br0"
  description = "Local Hypervior Bridge Interface."
}


variable "infra-nodes" {
    type = list
    description = "List of nodes and their attributes"
    default = [
      {
        "nodename"  = "nodename",
        "vcpu" = "2",
        "mem_in_gb" = "4",
        "sysdisk_in_gb" = 15,
        "datadisk_in_gb" = 20,
        "net_type" = "dhcp",
        "nodeip_with_mask" = "0.0.0.0/24",
        "nodeip_gateway" = "0.0.0.0"
     },
    ]
}
