proxmox-server = "192.168.2.250"
proxmox-user = "root"
#proxmox-password = "P3r1c0P4l0t3s"
proxmox-node = "pve"
proxmox-storage = "thin-2TB"
proxmox-storage-type = "lvmthin"




infra-environment = "labs"
#infra-os-base-template = "centos7-cloud-image"
infra-os-base-template = "focal-server-cloudimg-amd64" 

# Common Network Configuration
infra-network-name = "labs"
infra-network-domain = "labs"
infra-network-nameserver = "8.8.8.8"
#infra-network-subnet = ["192.168.202.0/23"] # Only for non-ip-static environments and should not exist in your host.
infra-network-bridge = true #


public_sshkey = "../ssh-keys/provision.pub"
kvm_pool = "labs"
kvm_bridge_interface = "vmbr0"

infra-nodes = [
      {
        "nodename"  = "node1",
        "vcpu" = "2",
        "mem_in_gb" = "4",
        "sysdisk_in_gb" = "10",
        "datadisk_in_gb" = "10"
        "net_type" = "static",
        "nodeip_with_mask" = "192.168.2.111/24",
        "nodeip_gateway" = "192.168.2.1"
     },
      {
        "nodename"  = "node2",
        "vcpu" = "2",
        "mem_in_gb" = "2",
        "sysdisk_in_gb" = "10",
        "datadisk_in_gb" = "10"
        "net_type" = "static",
        "nodeip_with_mask" = "192.168.2.112/24",
        "nodeip_gateway" = "192.168.2.1"
     },
]

