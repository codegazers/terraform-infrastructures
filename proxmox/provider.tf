provider "proxmox" {
    pm_api_url =  "https://${var.proxmox-server}:8006/api2/json"
    pm_user = "${var.proxmox-user}@pam"
    pm_password = var.proxmox-password
    pm_tls_insecure = "true"
    pm_parallel = 1
}