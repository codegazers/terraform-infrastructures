infra-lab = "labs"
#infra-os-base-url = "/virtual/base_images/labs/bionic64-base-image.img"
#infra-os-base-url = "https://cloud.centos.org/centos/8/x86_64/images/CentOS-8-GenericCloud-8.3.2011-20201204.2.x86_64.qcow2"
#infra-os-base-url = "/virtual/base_images/rhel-8.3-x86_64-kvm.qcow2"
#infra-os-base-url = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img"
infra-os-base-url = "/virtualization/base_images/focal-server-cloudimg-amd64.img"

# Common Network Configuration
infra-network-name = "labs"
infra-network-domain = "labs.local"
infra-network-nameserver = "8.8.8.8"
infra-network-interface = "ens3"
#infra-network-subnet = ["192.168.202.0/23"] # Only for non-ip-static environments and should not exist in your host.
infra-network-bridge = true #
infra-default-username = "vmuser"
infra-default-password = "vmuser"


kvm_pool = "labs"
kvm_bridge_interface = "br0"


# Configuration for each node
   # - We can avoid using specific mac address removing "mac" key.
   # - Default net_type is dhcp
   #         "net_type" = "static",
   #         "nodeip_with_mask" = "192.168.202.10/24"
