esxi_hostname="hq.hoplasoftware.com"
esxi_hostport="22"
esxi_hostssl="5443"
esxi_username="root"
#esxi_password="ASKMEPLEASE"

esxi_disk_store="DataStore2"

esxi_virtual_network="VM Network"

infra-environment = "labs"
infra-os-base-template = "focal-server-cloudimg-amd64"


# Common Network Configuration
infra-network-name = "labs"
infra-network-domain = "labs.local"
infra-network-nameserver = "8.8.8.8"
infra-network-interface = "ens33"
#infra-network-subnet = ["192.168.202.0/23"] # Only for non-ip-static environments and should not exist in your host.
infra-network-bridge = true #
infra-default-username = "vmuser"
infra-default-password = "vmuser"

private_key_path = "../ssh-keys/provision"



# Configuration for each node
   # - We can avoid using specific mac address removing "mac" key.
   # - Default net_type is dhcp
   #         "net_type" = "static",
   #         "nodeip_with_mask" = "192.168.202.10/24"
infra-nodes = [
      {
        "nodename"  = "node1",
        "vcpu" = "2",
        "mem_in_gb" = "4",
        "sysdisk_in_gb" = "10",
        "datadisk_in_gb" = "10"
        /* Does not work yet we only have dhcp
        "net_type" = "static",
        "mac" = "52:54:00:b2:2f:80",
        "nodeip_with_mask" = "192.168.201.110/23",
        "nodeip_gateway" = "192.168.201.1"
        */
     }, /*
     {
        "nodename"  = "node1",
        "vcpu" = "2",
        "mem_in_gb" = "6",
        "sysdisk_in_gb" = 15,
        "datadisk_in_gb" = 30,
        "net_type" = "static",
        "mac" = "52:54:00:b2:2f:81",
        "node_ipaddres" = "192.168.2.111",
        "node_mask" =  "255.255.255.0",
        "node_gateway" = "192.168.2.1"
     },
     {
        "nodename"  = "node2",
        "vcpu" = "2",
        "mem_in_gb" = "6",
        "sysdisk_in_gb" = 15,
        "datadisk_in_gb" = 30,
        "net_type" = "static",
        "mac" = "52:54:00:b2:2f:82",
        "node_ipaddres" = "192.168.2.112",
        "node_mask" =  "255.255.255.0",
        "node_gateway" = "192.168.2.1"
     },
     {
        "nodename"  = "node3",
        "vcpu" = "2",
        "mem_in_gb" = "6",
        "sysdisk_in_gb" = 15,
        "datadisk_in_gb" = 30,
        "net_type" = "static",
        "mac" = "52:54:00:b2:2f:83",
        "node_ipaddres" = "192.168.2.113",
        "node_mask" =  "255.255.255.0",
        "node_gateway" = "192.168.2.1"
     },*/
]
