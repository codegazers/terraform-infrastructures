variable "esxi_hostname" {
  default = "esxi"
}

variable "esxi_hostport" {
  default = "22"
}

variable "esxi_hostssl" {
  default = "443"
}

variable "esxi_username" {
  default = "root"
}

variable "esxi_password" { # Unspecified will prompt
}

variable "esxi_disk_store" {
  default="Datastore"
}

variable "esxi_virtual_network" {
  default = "VM Network"
}

variable "infra-os-base-template" {
  type = string
  default = "template"
  description = "Infrastructure OS Base Image"
}

variable "private_key_path" {
  type = string
  default = "../keys/provision"
  description = "SSH Private Key"
}

variable "infra-environment" {
  type = string
  default = "lab"
  description = "Infrastructure Environment Name"
}

variable "infra-network-name" {
  type = string
  default = "labs"
  description = "Network Name"
}

variable "infra-network-domain" {
  type = string
  default = "labs"
  description = "Nodes Network Domain"
}

variable "infra-network-nameserver" {
  type = string
  default = "8.8.8.8"
  description = "Nodes Network Nameserver"
}

variable "infra-network-interface" {
  type = string
  default = "eth0"
  description = "Nodes Network Interface (ens3 for Debian-like nodes and eth0 for RedHat-like nodes)"
}


variable "infra-network-subnet" {
  description = "Nodes' Subnet (For DHCP Only)"
  type = list(string)
  default = ["192.168.100.0/24"]
}

variable "infra-network-gateway" {
  type = string
  default = "192.168.100.1"
}


variable "infra-network-bridge" {
  description = "Enables libvirt Network Bridge Mode"
  type = bool
  default = false
}

variable "infra-default-username" {
  description = "Default username to be created (provision user will always be created)"
  type = string
  default = "vmuser"
}

variable "infra-default-password" {
  description = "Password for default user (provision user should be used with ssh-key)"
  type = string
  default = "vmuser"
}


variable "kvm_pool" {
  type = string
  default = "labs"
  description = "Pool for disk images and other resources (must be previously created)."

    # $ virsh pool-define-as --name labs --type dir --target /media/work/kvm
    # Pool default defined
    # Set pool to be started when libvirt daemons starts:

    # $ virsh pool-autostart labs
    # Pool default marked as autostarted
    # Start pool:

    # $ virsh pool-start labs
    # Pool default started

}

variable "kvm_bridge_interface" {
  type = string
  default = "br0"
  description = "Local Hypervior Bridge Interface."
}


variable "infra-nodes" {
    type = list
    description = "List of nodes and their attributes"
    default = [
      {
        "nodename"  = "nodename",
        "vcpu" = "2",
        "mem_in_gb" = "4",
        "sysdisk_in_gb" = 15,
        "datadisk_in_gb" = 20,
        "net_type" = "dhcp",
        "nodeip_with_mask" = "0.0.0.0/24"
     },
    ]
}
