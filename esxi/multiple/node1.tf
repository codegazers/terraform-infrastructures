

#########################################
#  Resource Pools
#########################################
resource "esxi_resource_pool" "Pool" {
  resource_pool_name = var.infra-environment
  cpu_min            = "100"
  cpu_min_expandable = "true"
  cpu_max            = "8000"
  cpu_shares         = "normal"
  mem_min            = "200"
  mem_min_expandable = "false"
  mem_max            = "8192"
  mem_shares         = "normal"
}

# resource "esxi_resource_pool" "pool2" {
#   resource_pool_name = "${esxi_resource_pool.Pool.resource_pool_name}/pool2"
# }

#########################################
#  Virtual Disks
#########################################
resource "esxi_virtual_disk" "vdisk1" {
  virtual_disk_disk_store = var.esxi_disk_store
  virtual_disk_dir        = "Terraform"
  virtual_disk_name       = "vdisk_1.vmdk"
  virtual_disk_size       = 10
  virtual_disk_type       = "zeroedthick"
}

# resource "esxi_virtual_disk" "vdisk2" {
#   virtual_disk_disk_store = var.esxi_disk_store
#   virtual_disk_dir        = "Terraform"
#   virtual_disk_size       = 15
#   virtual_disk_type       = "eagerzeroedthick"
# }

#########################################
#  ESXI Guest resource
#########################################
#
#  This Guest VM is a clone of an existing Guest VM named "centos7" (must exist and
#  be powered off), located in the "Templates" resource pool.  vmtest03 will be powered
#  on by default by terraform.  The virtual network "VM Network", must already exist on
#  your esxi host!
#
resource "esxi_guest" "node1" {
  guest_name = "${var.infra-environment}-node1"
  disk_store = var.esxi_disk_store
  #guestos    = "centos-64"




  boot_disk_type = "thin"
  boot_disk_size = "35"

  memsize            = "2048"
  numvcpus           = "2"
  #resource_pool_name = esxi_resource_pool.pool2.resource_pool_name
  power              = "on"

  clone_from_vm = var.infra-base_template


  network_interfaces {
    virtual_network = "VM Network"
    mac_address     = "00:50:56:a1:b1:c3"
    nic_type        = "e1000"
  }

  guest_startup_timeout  = 45
  guest_shutdown_timeout = 30

  virtual_disks {
    virtual_disk_id = esxi_virtual_disk.vdisk1.id
    slot            = "0:1"
  }
  # virtual_disks {
  #   virtual_disk_id = esxi_virtual_disk.vdisk2.id
  #   slot            = "0:2"
  # }
}
