# DEBUG
#TF_LOG=TRACE terraform apply -auto-approve

resource "null_resource" "tmp_directories" {

  provisioner "local-exec" {
    command = <<-EOF
      mkdir -p "${path.module}/tmpfiles/"
    EOF
  }

  provisioner "local-exec" {
    when    = destroy
    command = <<-EOF
      rm -rf "${path.module}/tmpfiles/"
    EOF
  }

}

#resource "esxi_resource_pool" "Terraform" {
#  resource_pool_name = "Terraform"
#  cpu_min  = "100"
#  cpu_min_expandable = "true"
#  cpu_max  = "8000"
#  cpu_shares         = "normal"
#  mem_min  = "200"
#  mem_min_expandable = "false"
#  mem_max  = "8192"
#  mem_shares         = "normal"
#}

#resource "esxi_resource_pool" "pool2" {
#  resource_pool_name = "${esxi_resource_pool.Terraform.resource_pool_name}/pool2"
#}


/* Not working yet
data "template_file" "user_data" {
  count = length(var.infra-nodes)
  template = file("${path.module}/cloudinit_user.cfg")
    vars = {
    Domain = var.infra-network-domain
    Hostname = lookup(var.infra-nodes[count.index], "nodename")
    Username = var.infra-default-username
    Password = var.infra-default-password
  }
}

data "template_file" "network_config" {
  count = length(var.infra-nodes)
  template = file("${path.module}/cloudinit_net_${lookup(var.infra-nodes[count.index], "net_type", "dhcp")}.cfg")
  vars = {
    Domain = var.infra-network-domain
    NameServer = var.infra-network-nameserver
    Interface = var.infra-network-interface
    Gateway = lookup(var.infra-nodes[count.index], "nodeip_gateway","use_dhcp")
    IPAddress = lookup(var.infra-nodes[count.index], "nodeip_with_mask", "use_dhcp")
  }
}


resource "local_file" "cloudinit_net_file" {
  depends_on = [
    null_resource.tmp_directories,
  ]
  
  filename = "${path.module}/tmpfiles/${lookup(var.infra-nodes[count.index], "nodename")}-cloudinit_net.cfg"
  count = length(var.infra-nodes)
  content = data.template_file.network_config[count.index].rendered
  
  provisioner "local-exec" {
    command = "cat > ${path.module}/tmpfiles/${lookup(var.infra-nodes[count.index], "nodename")}-cloudinit_net.cfg <<EOL\n${data.template_file.network_config[count.index].rendered}\nEOL"
  }
}
*/


resource "esxi_virtual_disk" "data" {
     count = length(var.infra-nodes)
     virtual_disk_disk_store = var.esxi_disk_store
     virtual_disk_dir        = lookup(var.infra-nodes[count.index], "nodename")
     virtual_disk_name       = "${var.infra-environment}-${lookup(var.infra-nodes[count.index], "nodename")}-data.vmdk"
     virtual_disk_size       = lookup(var.infra-nodes[count.index], "datadisk_in_gb")
     virtual_disk_type       = "eagerzeroedthick"
  }


resource "esxi_guest" "node" {

#  resource_pool_name = esxi_resource_pool.pool2.resource_pool_name

    guest_name = "${var.infra-environment}-${lookup(var.infra-nodes[count.index], "nodename")}"
    
    memsize = lookup(var.infra-nodes[count.index], "mem_in_gb") * 1024

    numvcpus = lookup(var.infra-nodes[count.index], "vcpu")

    disk_store = var.esxi_disk_store

    clone_from_vm = var.infra-os-base-template

    power= "on"

    boot_disk_type = "thin"
    boot_disk_size = lookup(var.infra-nodes[count.index], "sysdisk_in_gb")

    network_interfaces {
        virtual_network = var.esxi_virtual_network
        mac_address     = lookup(var.infra-nodes[count.index], "mac", null)
        nic_type        = "e1000"
    }

/* Not working yet
    guestinfo = {
        "metadata.encoding" = "gzip+base64"
        "metadata"          = base64gzip(local_file.cloudinit_net_file[count.index].content)
    }
*/

    virtual_disks {
         virtual_disk_id = esxi_virtual_disk.data[count.index].id
         slot   = "0:1"
    }
    

  count = length(var.infra-nodes)

}


output "ips" {
    value = [esxi_guest.node.*.ip_address]
}